# gl-instance-chrome

Clone this project so that it can be added as an extension in developer mode.

1. Open the Extension Management page by navigating to `chrome://extensions`.
1. Enable Developer Mode by clicking the toggle switch next to `Developer mode`.
1. Click the `LOAD UNPACKED` button and select the extension directory.

![screenshot](https://gitlab.com/gitlab-com/gl-infra/gl-instance-chrome/raw/master/doc/enable-developer-mode.png)
