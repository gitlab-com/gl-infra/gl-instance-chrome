'use strict';

let bkg = chrome.extension.getBackgroundPage();

let instances = ['dev', 'ops', 'security'];
let projects = [
    {
        canonical: "/gitlab-org/gitlab-foss",
        dev: "/gitlab/gitlabhq",
        security: "/gitlab-org/security/gitlab-foss"
    },
    {
        canonical: "/gitlab-org/release-tools",
        ops: "/gitlab-org/release/tools"
    },
    {
      canonical: "/gitlab-com/chatops",
      ops: "/gitlab-com/chatops"
    },
    {
        canonical: "/gitlab-org/merge-train",
        ops: "/gitlab-org/merge-train"
    },
    {   // matches both deployer and deploy-tooling
        canonical: "/gitlab-com/gl-infra/deploy",
        ops: "/gitlab-com/gl-infra/deploy"
    },
    {
        canonical: "/gitlab-org/gitlab",
        dev: "/gitlab/gitlab-ee",
        security: "/gitlab-org/security/gitlab"
    },
    {
        canonical: "/gitlab-org/charts/",
        dev: "/gitlab/charts/",
        security: "/gitlab-org/security/charts/"
    },
    {
        canonical: "/gitlab-org/build/CNG",
        dev: "/gitlab/charts/components/images",
        security: "/gitlab-org/security/charts/components/images"
    },
    {
        canonical: "/gitlab-org/quality/release",
        ops: "/gitlab-org/quality/release"
    },
    {
        canonical: "/gitlab-org/",
        dev: "/gitlab/",
        security: "/gitlab-org/security",
    },
    {
        canonical: "/gitlab-com/www-gitlab-com",
        dev: "/gitlab/www-gitlab-com"
    },
    {
        canonical: "/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles",
        ops: "/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles"
    },
];

let convertUrl = function(aUrl, instance) {
    let url = new URL(aUrl);

    switch (instance) {
    case 'canonical':
    case 'security':
        break;
    case 'dev':
        url.host = "dev.gitlab.org";
        break;
    case 'ops':
        url.host = "ops.gitlab.net";
        break;
    default:
        bkg.console.log("Unknown instance". instance);

        return null;
    }

    let path = url.pathname;

    for(let project of projects) {
        if (path.startsWith(project.canonical)) {
            let newPath = project[instance];

            if (newPath == null) {
                return null;
            }

            url.pathname = newPath + path.slice(project.canonical.length);

            return url.toString();
        }
    }

    return null;
};

chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    let myTab = tabs[0];

    for(let instance of instances) {
        let url = convertUrl(myTab.url, instance);
        let element = document.getElementById(instance);

        if (url != null) {
            element.onclick = function() {
                chrome.tabs.create(
                    {
                        openerTabId: myTab.id,
                        url: url,
                        active: true
                    }
                );
            };
        } else {
            element.style.display = "none";
        }
    }
});
