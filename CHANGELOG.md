# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- ChatOps repository
- release.gitlab.net quality repo
- k8s-workloads/gitlab-helmfiles repo

## [0.2.0] - 2020-05-06
### Added
- Helm charts and CNG repositories
- Switch from canonical to security mirrors
- Switch from canonical to ops instance
- release-tools, merge-train, deployer, and deploy-tooling repositories

## [0.1.0] - 2019-12-13
### Added
- Switch from gitlab.com to dev instance

[Unreleased]: https://gitlab.com/gitlab-com/gl-infra/gl-instance-chrome/-/compare/v0.2.0...master
[0.2.0]: https://gitlab.com/gitlab-com/gl-infra/gl-instance-chrome/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/gitlab-com/gl-infra/gl-instance-chrome/-/tags/v0.1.0
